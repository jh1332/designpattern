﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : Widget
{
	public Button(Widget _widget, string _helpMsg) : base(_widget, _helpMsg)
	{

	}

	override public void HandleHelp()
	{
		if(HasHelp())
		{
			ShowHelpMsg();
		}
		else
		{
			base.HandleHelp();
		}
	}
}
