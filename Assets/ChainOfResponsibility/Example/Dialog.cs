﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialog : Widget
{
	public Dialog(HelpHandler _successor, string _helpMsg) : base(null, _helpMsg)
	{
		SetHandler(_successor, _helpMsg);
	}

	public override void HandleHelp()
	{
		
	}
}
