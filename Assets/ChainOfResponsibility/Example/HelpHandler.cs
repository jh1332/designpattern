﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpHandler
{
	HelpHandler _successor = null;
	string _helpMsg = string.Empty;

	public HelpHandler(HelpHandler successor, string helpMsg)
	{
		SetHandler(successor, helpMsg);
	}

	public void SetHandler(HelpHandler successor, string helpMsg)
	{
		_successor = successor;
		_helpMsg = helpMsg;
	}

	virtual public void HandleHelp()
	{
		if(null != _successor)
		{
			_successor.HandleHelp();
		}
	}

	virtual protected bool HasHelp()
	{
		return !_helpMsg.Equals(string.Empty);
	}

	protected virtual void ShowHelpMsg()
	{
		Debug.Log(_helpMsg);
	}
}
